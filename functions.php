<?php
/**
 * MOOSH Starter.
 *
 * This file adds functions to the MOOSH Starter Theme.
 *
 * @package MOOSH Starter
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    https://mooshcreative.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'moosh_starter_localization_setup' );
function moosh_starter_localization_setup(){
	load_child_theme_textdomain( 'moosh-starter', get_stylesheet_directory() . '/languages' );
}

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', 'MOOSH Starter' );
define( 'CHILD_THEME_URL', 'https://mooshcreative.com/' );
define( 'CHILD_THEME_VERSION', '1.0.0' );

// Remove unnecessary scripts.
add_action( 'init', 'moosh_starter_clean_header' );
function moosh_starter_clean_header(){
	wp_deregister_script( 'jquery' );
}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );
